import React,{useState, useEffect} from 'react'
import axios from "axios"
import { useNavigate, useParams } from "react-router-dom"



function EditUser() {
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [experience, setExperience] = useState(0)
  const navigate = useNavigate()
  const {id} = useParams()

  useEffect(() => {
    getUserbyId()
  },[])

  

  const updateUser = async (e) => {
    try {
      e.preventDefault()
      await axios.put(`http://localhost:4000/api/v1/players/${id}`,{
        username,
        email,
        password,
        experience
    })
    navigate('/');
    console.log(`update has been executed`)
    } catch (error) {
      console.log(error)
    }
  }

  const getUserbyId = async() => {
    const response = await axios.get(`http://localhost:4000/api/v1/players/${id}`)
    setUsername(response.data.data.username)
    setEmail(response.data.data.email)
    setPassword(response.data.data.password)
    setExperience(response.data.data.experience)
  }

  return (
    <form onSubmit={updateUser}>
  <div className="mb-3">
    <label className="form-label">Username</label>
    <input 
      type="username" 
      className="form-control" 
      value={username} 
      onChange={(e) => setUsername(e.target.value)}
    />

  </div>
  <div className="mb-3">
    <label className="form-label">Email address</label>
    <input 
      type="email" 
      className="form-control" 
      value={email} 
      onChange={(e) => setEmail(e.target.value)}
    />

  </div>
  <div className="mb-3">
    <label className="form-label">Password</label>
    <input 
      type="password" 
      className="form-control" 
      value={password} 
      onChange={(e) => setPassword(e.target.value)}
    />
  </div>
  <div className="mb-3">
    <label className="form-label">Experience</label>
    <input 
      type="number" 
      className="form-control" 
      value={experience} 
      onChange={(e) => setExperience(e.target.value)}
    />
  </div>
  <button type="submit" className="btn btn-primary">Update</button>
</form>
  )
}

export default EditUser